package br.gov.fazenda.messaging.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeMessaging {
    @JsonProperty(value = "id_nfe")
    private Long id;

    @JsonProperty(value = "cpf_cnpj")
    private String cpfCnpj;

    @JsonProperty(value = "valor")
    private double amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
