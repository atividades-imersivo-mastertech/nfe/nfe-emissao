package br.gov.fazenda.nfeemissao.controller;

import br.gov.fazenda.nfeemissao.dto.request.NfeDetailRequest;
import br.gov.fazenda.nfeemissao.dto.request.NfeRequestRequest;
import br.gov.fazenda.nfeemissao.dto.response.NfeRequestedResponse;
import br.gov.fazenda.nfeemissao.model.NfeDetail;
import br.gov.fazenda.nfeemissao.model.NfeRequest;
import br.gov.fazenda.nfeemissao.dto.NfeRequestMapper;
import br.gov.fazenda.nfeemissao.dto.response.NfeRequestStatusResponse;
import br.gov.fazenda.nfeemissao.service.NfeRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class NfeRequestController {
    @Autowired
    private NfeRequestService nfeRequestService;

    @Autowired
    private NfeRequestMapper mapper;

    @PostMapping("/emitir")
    public NfeRequestStatusResponse emmitNfe(@RequestBody NfeRequestRequest nfeRequestRequest) {
        NfeRequest nfe = mapper.toNfeRequest(nfeRequestRequest);
        nfe = nfeRequestService.sendNfe(nfe);
        NfeRequestStatusResponse nfeRequestStatusResponse = mapper.toNfeRequesteStatusResponse(nfe);
        return nfeRequestStatusResponse;
    }

    @PutMapping("/attach/{id_nfeRequest}")
    public void attachNfeDetail(@RequestBody NfeDetailRequest nfeDetailRequest, @PathVariable("id_nfeRequest") Long idNfeRequest) {
        NfeRequest nfeRequest = nfeRequestService.findById(idNfeRequest);
        NfeDetail nfeDetail = nfeRequestService.saveNfeDetail(mapper.toNfeDetail(nfeDetailRequest));
        nfeRequestService.updateNfeRequest(nfeRequest,nfeDetail);
    }

    @GetMapping("/consultar/{cpf_cnpj}")
    public List<NfeRequestedResponse> listByCnpj(@PathVariable("cpf_cnpj") String cpfCnpj) {
        List<NfeRequestedResponse> requestedResponseList = new ArrayList<>();

        List<NfeRequest> nfeRequestList = nfeRequestService.listByCpfCnpj(cpfCnpj);
        nfeRequestList.forEach(nfe -> {
            NfeRequestedResponse requestedResponse = mapper.toNfeRequestedRessponse(nfe);
            requestedResponseList.add(requestedResponse);
        });

        return requestedResponseList;
    }
}
