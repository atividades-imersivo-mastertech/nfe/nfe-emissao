package br.gov.fazenda.nfeemissao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NfeEmissaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NfeEmissaoApplication.class, args);
	}

}
