package br.gov.fazenda.nfeemissao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Solicitação de nota fiscal eletrônica não encontrada")
public class NfeRequestNotFoundException extends RuntimeException  {
}
