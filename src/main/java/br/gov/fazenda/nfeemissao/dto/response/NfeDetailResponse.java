package br.gov.fazenda.nfeemissao.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeDetailResponse {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("valorInicial")
    private Double initialAmount;

    @JsonProperty("valorIRRF")
    private Double irrfAmount;

    @JsonProperty("valorCSSL")
    private Double csslAmount;

    @JsonProperty("valorCofins")
    private Double cofinsAmount;

    @JsonProperty("valorFinal")
    private Double finalAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getIrrfAmount() {
        return irrfAmount;
    }

    public void setIrrfAmount(Double irrfAmount) {
        this.irrfAmount = irrfAmount;
    }

    public Double getCsslAmount() {
        return csslAmount;
    }

    public void setCsslAmount(Double csslAmount) {
        this.csslAmount = csslAmount;
    }

    public Double getCofinsAmount() {
        return cofinsAmount;
    }

    public void setCofinsAmount(Double cofinsAmount) {
        this.cofinsAmount = cofinsAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }
}
