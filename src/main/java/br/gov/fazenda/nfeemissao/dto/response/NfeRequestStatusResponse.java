package br.gov.fazenda.nfeemissao.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeRequestStatusResponse {
    @JsonProperty("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
