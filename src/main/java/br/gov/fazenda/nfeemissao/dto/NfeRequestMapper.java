package br.gov.fazenda.nfeemissao.dto;

import br.gov.fazenda.nfeemissao.dto.request.NfeDetailRequest;
import br.gov.fazenda.nfeemissao.dto.request.NfeRequestRequest;
import br.gov.fazenda.nfeemissao.dto.response.NfeDetailResponse;
import br.gov.fazenda.nfeemissao.dto.response.NfeRequestedResponse;
import br.gov.fazenda.nfeemissao.model.NfeDetail;
import br.gov.fazenda.nfeemissao.model.NfeRequest;
import br.gov.fazenda.nfeemissao.dto.response.NfeRequestStatusResponse;
import org.springframework.stereotype.Component;

@Component
public class NfeRequestMapper {
    public NfeRequest toNfeRequest(NfeRequestRequest nfeRequestRequest) {
        NfeRequest nfe = new NfeRequest();

        nfe.setCpfCnpj(nfeRequestRequest.getCpfCnpj());
        nfe.setAmount(nfeRequestRequest.getAmount());

        return nfe;
    }

    public NfeRequestStatusResponse toNfeRequesteStatusResponse(NfeRequest nfeRequest) {
        NfeRequestStatusResponse nfeRequestStatusResponse = new NfeRequestStatusResponse();
        nfeRequestStatusResponse.setStatus(nfeRequest.getStatus());
        return nfeRequestStatusResponse;
    }

    public NfeRequestedResponse toNfeRequestedRessponse(NfeRequest nfeRequest) {
        NfeRequestedResponse requestedResponse = new NfeRequestedResponse();

        requestedResponse.setIdnetity(nfeRequest.getCpfCnpj());
        requestedResponse.setAmount(nfeRequest.getAmount());
        requestedResponse.setStatus(nfeRequest.getStatus());
        requestedResponse.setNfe(toNfeDetailResponse(nfeRequest.getNfeDetail()));

        return requestedResponse;
    }

    private NfeDetailResponse toNfeDetailResponse(NfeDetail nfeDetail) {
        if(nfeDetail == null) return null;
        NfeDetailResponse detailResponse = new NfeDetailResponse();

        detailResponse.setId(nfeDetail.getId());
        detailResponse.setCsslAmount(nfeDetail.getCSSLAmount());
        detailResponse.setIrrfAmount(nfeDetail.getIRRFAmount());
        detailResponse.setCofinsAmount(nfeDetail.getCofinsAmount());
        detailResponse.setFinalAmount(nfeDetail.getFinalAmount());

        return  detailResponse;
    }

    public NfeDetail toNfeDetail(NfeDetailRequest detailRequest) {
        NfeDetail nfeDetail = new NfeDetail();

        nfeDetail.setInitialAmount(detailRequest.getInitialAmount());
        nfeDetail.setCSSLAmount(detailRequest.getCSSLAmount());
        nfeDetail.setIRRFAmount(detailRequest.getIRRFAmount());
        nfeDetail.setCofinsAmount(detailRequest.getCofinsAmount());
        nfeDetail.setFinalAmount(detailRequest.getFinalAmount());

        return nfeDetail;
    }
}
