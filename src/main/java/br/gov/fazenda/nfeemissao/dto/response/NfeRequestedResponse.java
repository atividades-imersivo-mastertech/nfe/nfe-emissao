package br.gov.fazenda.nfeemissao.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeRequestedResponse {
    @JsonProperty("identidade")
    private String idnetity;

    @JsonProperty("valor")
    private Double amount;

    @JsonProperty("status")
    private String status;

    @JsonProperty("nfe")
    private NfeDetailResponse nfe;

    public String getIdnetity() {
        return idnetity;
    }

    public void setIdnetity(String idnetity) {
        this.idnetity = idnetity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NfeDetailResponse getNfe() {
        return nfe;
    }

    public void setNfe(NfeDetailResponse nfe) {
        this.nfe = nfe;
    }
}
