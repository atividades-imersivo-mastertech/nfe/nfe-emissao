package br.gov.fazenda.nfeemissao.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeDetailRequest {
    @JsonProperty("valorInicial")
    private Double initialAmount;

    @JsonProperty("valorIRRF")
    private Double IRRFAmount;

    @JsonProperty("valorCSSL")
    private Double CSSLAmount;

    @JsonProperty("valorCofins")
    private Double cofinsAmount;

    @JsonProperty("valorFinal")
    private Double finalAmount;

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getIRRFAmount() {
        return IRRFAmount;
    }

    public void setIRRFAmount(Double IRRFAmount) {
        this.IRRFAmount = IRRFAmount;
    }

    public Double getCSSLAmount() {
        return CSSLAmount;
    }

    public void setCSSLAmount(Double CSSLAmount) {
        this.CSSLAmount = CSSLAmount;
    }

    public Double getCofinsAmount() {
        return cofinsAmount;
    }

    public void setCofinsAmount(Double cofinsAmount) {
        this.cofinsAmount = cofinsAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }
}
