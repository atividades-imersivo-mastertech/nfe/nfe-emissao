package br.gov.fazenda.nfeemissao.dto;

import br.gov.fazenda.messaging.model.NfeMessaging;
import br.gov.fazenda.nfeemissao.model.NfeRequest;
import org.springframework.stereotype.Component;

@Component
public class NfeMessagingMapper {

    public NfeMessaging toNfeMessaging(NfeRequest nfeRequest) {
        NfeMessaging nfeMessaging = new NfeMessaging();
        nfeMessaging.setId(nfeRequest.getId());
        nfeMessaging.setAmount(nfeRequest.getAmount());
        nfeMessaging.setCpfCnpj(nfeRequest.getCpfCnpj());
        return nfeMessaging;
    }
}
