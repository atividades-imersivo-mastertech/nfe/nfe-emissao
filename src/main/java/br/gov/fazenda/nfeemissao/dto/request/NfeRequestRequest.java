package br.gov.fazenda.nfeemissao.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NfeRequestRequest {
    @JsonProperty(value = "identidade")
    private String cpfCnpj;
    @JsonProperty(value = "valor")
    private Double amount;

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
