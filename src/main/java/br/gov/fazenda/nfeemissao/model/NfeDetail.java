package br.gov.fazenda.nfeemissao.model;

import javax.persistence.*;

@Entity
@Table(name = "NOTA_DETALHE")
public class NfeDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "valor_inicial")
    private Double initialAmount;

    @Column(name = "valor_irrf")
    private Double IRRFAmount;

    @Column(name = "valor_CSSL")
    private Double CSSLAmount;

    @Column(name = "valor_cofins")
    private Double cofinsAmount;

    @Column(name = "valor_final")
    private Double finalAmount;

    @OneToOne(mappedBy = "nfeDetail")
    NfeRequest nfeRequest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(Double initialAmount) {
        this.initialAmount = initialAmount;
    }

    public Double getIRRFAmount() {
        return IRRFAmount;
    }

    public void setIRRFAmount(Double IRRFAmount) {
        this.IRRFAmount = IRRFAmount;
    }

    public Double getCSSLAmount() {
        return CSSLAmount;
    }

    public void setCSSLAmount(Double CSSLAmount) {
        this.CSSLAmount = CSSLAmount;
    }

    public Double getCofinsAmount() {
        return cofinsAmount;
    }

    public void setCofinsAmount(Double cofinsAmount) {
        this.cofinsAmount = cofinsAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }
}
