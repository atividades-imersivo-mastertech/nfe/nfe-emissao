package br.gov.fazenda.nfeemissao.model;

import javax.persistence.*;

@Entity()
@Table(name = "REGISTO_NOTA")
public class NfeRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cpf_cnpj")
    private String cpfCnpj;

    @Column(name = "valor_nota")
    private Double amount;

    @Column(name = "status_nota")
    private String status;

    @OneToOne
    @JoinColumn(name = "id_nota_detalhe")
    private NfeDetail nfeDetail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public NfeDetail getNfeDetail() {
        return nfeDetail;
    }

    public void setNfeDetail(NfeDetail nfeDetail) {
        this.nfeDetail = nfeDetail;
    }
}
