package br.gov.fazenda.nfeemissao.service;

import br.gov.fazenda.messaging.model.NfeGenericLog;
import br.gov.fazenda.messaging.model.NfeMessaging;
import br.gov.fazenda.nfeemissao.dto.NfeMessagingMapper;
import br.gov.fazenda.nfeemissao.exceptions.NfeRequestNotFoundException;
import br.gov.fazenda.nfeemissao.model.NfeDetail;
import br.gov.fazenda.nfeemissao.model.NfeRequest;
import br.gov.fazenda.nfeemissao.repository.NfeDetailRepository;
import br.gov.fazenda.nfeemissao.repository.NfeRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class NfeRequestService {
    @Autowired
    private NfeRequestRepository repository;

    @Autowired
    private NfeDetailRepository detailRepository;

    @Autowired
    private NfeMessagingMapper messagingMapper;

    @Autowired
    private KafkaTemplate<String, NfeMessaging> sender;

    @Autowired
    private KafkaTemplate<String, NfeGenericLog> logSender;


    public NfeRequest sendNfe(NfeRequest nfeRequest) {
        nfeRequest.setStatus("pending");
        NfeRequest savedNfeRequest = repository.save(nfeRequest);

        NfeMessaging nfeMessaging = messagingMapper.toNfeMessaging(savedNfeRequest);

        sender.send("kaique-biro-1", "kaiquera", nfeMessaging);
        logSender.send("kaique-biro-2", "kaiqueraLogger", new NfeGenericLog(new Date(), "Requisição",
                "Foi requisitada uma nota fiscal para " + nfeRequest.getCpfCnpj()));
        return savedNfeRequest;
    }

    public void updateNfeRequest(NfeRequest nfeRequest, NfeDetail nfeDetail) {
        nfeRequest.setNfeDetail(nfeDetail);

        if (nfeRequest.getNfeDetail() != null) {
            nfeRequest.setStatus("complete");
        }

        repository.save(nfeRequest);
        logSender.send("kaique-biro-2", "kaiqueraLogger", new NfeGenericLog(new Date(), "Registro",
                "A nota "+ nfeRequest.getId() + " foi atualizada"));
    }

    public NfeDetail saveNfeDetail(NfeDetail detail) {
        return detailRepository.save(detail);
    }

    public List<NfeRequest> listByCpfCnpj(String cpfCnpj) {
        List<NfeRequest> nfeRequestList = repository.findByCpfCnpj(cpfCnpj);
        logSender.send("kaique-biro-2", "kaiqueraLogger", new NfeGenericLog(new Date(), "Registro",
                cpfCnpj + " consultou seus registros"));
        return nfeRequestList;
    }

    public NfeRequest findById(Long id) {
        Optional<NfeRequest> nfeRequest = repository.findById(id);
        if (!nfeRequest.isPresent()) {
            throw new NfeRequestNotFoundException();
        }
        return nfeRequest.get();
    }
}
