package br.gov.fazenda.nfeemissao.repository;


import br.gov.fazenda.nfeemissao.model.NfeDetail;
import org.springframework.data.repository.CrudRepository;

public interface NfeDetailRepository extends CrudRepository<NfeDetail, Long> {
}
