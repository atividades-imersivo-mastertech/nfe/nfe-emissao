package br.gov.fazenda.nfeemissao.repository;

import br.gov.fazenda.nfeemissao.model.NfeRequest;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NfeRequestRepository extends CrudRepository<NfeRequest, Long> {
    List<NfeRequest> findByCpfCnpj(String cpfCnpj);
}
